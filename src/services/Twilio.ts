/**
 * Define Twilio OAuth2
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import Log from '../middlewares/Log';

const twilio = require('twilio');

class Twilio {
  static senderPhoneNumber = process.env.TWILIO_SENDER_PHONE_NUMBER;
  static client = null;

  public static init(): any {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;
    this.senderPhoneNumber = process.env.TWILIO_SENDER_PHONE_NUMBER;
    this.client = twilio(accountSid, authToken);
  }

  public static async sendSMS(sendeePhone: string, otp: string): Promise<string> {
    try {
      const response = await this.client.messages.create({
        body: `Your OTP is ${otp}`,
        from: this.senderPhoneNumber,
        to: sendeePhone
      });
      return 'SMS sent';
    } catch (_err) {
      Log.error(_err.stack);
      return 'Internal Server Error: Twilio';
    }
  }
}

export default Twilio;
