import Log from '../middlewares/Log';
import mongoose from '../providers/Database';

// TODO: refine
export const ensureIndexes = (_model: mongoose.Model<any, {}>) => {
  _model.ensureIndexes(function (err) {
    if (err) {
      Log.error(err.stack);
      throw err;
    }
  });
};
