/**
 * Define User Relationship model
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { ObjectId } from 'bson';
import IUserRelationship from '../interfaces/models/userRelationship';
import mongoose from '../providers/Database';
import { ensureIndexes } from './utils';

// Create the model schema & register your custom methods here
export interface IUserRelationshipModel extends IUserRelationship, mongoose.Document {}

// Define the Question Schema
export const UserRelationshipSchema = new mongoose.Schema({
  userFrom: { type: ObjectId, required: true, index: true },
  relationship: { type: String, required: true },
  userTo: { type: ObjectId, required: true, index: true }
});

const UserRelationship = mongoose.model<IUserRelationshipModel>('UserRelationship', UserRelationshipSchema);
ensureIndexes(UserRelationship);

export default UserRelationship;
