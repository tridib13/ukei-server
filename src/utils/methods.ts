export const cannotBeBlank = (field: string): string => `${field} cannot be blank`;
