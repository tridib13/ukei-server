/**
 * Define interface for User Relationship Model
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { ObjectId } from 'bson';

export enum Relationship {
  LIKE = 'like',
  NEUTRAL = 'neutral',
  DISLIKE = 'dislike',
  MATCH = 'match'
}

export default interface IUserRelationship {
  userFrom: ObjectId;
  relationship: String;
  userTo: ObjectId;
}
