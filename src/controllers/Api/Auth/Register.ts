/**
 * Define the Register API logic
 *
 * @author Faiz A. Farooqui <faiz@geekyants.com>
 */

import { IRequest, IResponse } from '../../../interfaces/vendors';
import User from '../../../models/User';
import { signToken } from '../../../middlewares/Jwt';
import { OTP } from '../../../interfaces/models/user';

interface IRequestBody {
  email: string;
  password: string;
  confirmPassword: string;
}

class Register {
  public static async perform(req: IRequest, res: IResponse): Promise<any> {
    const reqBody: IRequestBody = req.body;

    req.assert('email', 'E-mail cannot cannot be blank').notEmpty();
    req.assert('email', 'E-mail is not valid').isEmail();
    req.assert('password', 'Password cannot be blank').notEmpty();
    req.assert('password', 'Password length must be atleast 8 characters').isLength({ min: 8 });
    req.assert('confirmPassword', 'Confirmation Password cannot be blank').notEmpty();
    req.assert('confirmPassword', 'Password & Confirmation password does not match').equals(req.body.password);
    req.sanitize('email').normalizeEmail({ gmail_remove_dots: false });

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    const _email: string = reqBody.email;
    const _password: string = reqBody.password;

    try {
      const existingUser = await User.findOne({ email: _email });

      // TODO: custom error message (400 error)
      if (existingUser) {
        return res.status(400).send('Account with the e-mail address already exists.');
      }

      const user = new User({
        email: _email,
        password: _password,
        otp: OTP.NotVerified
      });

      user.save();

      const token = signToken(user._id.toString());

      return res.json({ user, token, token_expires_in: res.locals.app.jwtExpiresIn * 60 });
    } catch (err) {
      return res.status(500).send(err.toString());
    }
  }
}

export default Register;
