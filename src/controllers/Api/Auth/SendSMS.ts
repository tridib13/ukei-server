/**
 * Define the Verification SMS API logic
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { IRequest, IResponse, INext } from '../../../interfaces/vendors';
import User from '../../../models/User';
import Twilio from '../../../services/Twilio';
import { OTP } from '../../../interfaces/models/user';

interface IRequestBody {
  phone: string;
}

class SendSMS {
  public static async perform(req: IRequest, res: IResponse): Promise<any> {
    const reqBody: IRequestBody = req.body;

    const regex = /\+852\s?[0-9]{4}\s?[0-9]{4}/;
    req.assert('phone', 'Phone number cannot be blank').notEmpty();
    req.assert('phone', 'Phone number is not of required format. Format would be +852 1234 5678').matches(regex);

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    const _userId = req.user.userId;
    const _phone = reqBody.phone;
    const _otp: string = Math.floor(Math.random() * 899999 + 100000).toString();

    try {
      const existingUser = await User.findOne({ _id: _userId });

      // TODO: custom error message (500 error)
      if (!existingUser) {
        return res.status(500).send('Account does not exist.');
      }

      // TODO: custom error message (500 error)
      if (existingUser.otp === OTP.Verified) {
        return res.status(500).send('User has already been verified.');
      }

      const updatedUser = await User.findOneAndUpdate({ _id: _userId }, { otp: _otp });
      const response = await Twilio.sendSMS(_phone, _otp);

      return res.json(updatedUser);
    } catch (err) {
      return res.status(500).send(err.toString());
    }
  }
}

export default SendSMS;
