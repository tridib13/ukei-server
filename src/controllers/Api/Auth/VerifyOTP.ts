/**
 * Define the Verification SMS API logic
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { OTP } from '../../../interfaces/models/user';
import { IRequest, IResponse, INext } from '../../../interfaces/vendors';
import User from '../../../models/User';

interface IRequestBody {
  email: string;
  otp: string;
}

class VerifyOTP {
  public static async perform(req: IRequest, res: IResponse): Promise<any> {
    const reqBody: IRequestBody = req.body;

    req.assert('email', 'Email is empty!').notEmpty();
    req.assert('otp').matches(/[0-9]{6}/);

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    const _email = reqBody.email;
    const _otp = reqBody.otp;

    try {
      const existingUser = await User.findOne({ email: _email });

      // TODO: custom error message (500 error)
      if (!existingUser) {
        return res.status(500).send('Account does not exist.');
      }

      // TODO: custom error message (400 error)
      if (existingUser.otp !== _otp) {
        return res.status(400).send('OTP does not match!');
      }

      const updatedUser = await User.findOneAndUpdate({ email: _email }, { otp: OTP.Verified });

      return res.json(updatedUser);
    } catch (err) {
      return res.status(500).send(err.toString());
    }
  }
}

export default VerifyOTP;
