/**
 * Define Update User Relationsip API
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { Relationship } from '../../../interfaces/models/userRelationship';
import { cannotBeBlank } from '../../../utils/methods';
import UserRelationship from '../../../models/UserRelationship';
import Log from '../../../middlewares/Log';
import { ObjectId } from 'bson';
import { checkForMatch, checkForUnmatch } from './helper';

interface IRequestBody {
  userFrom: string;
  relationship: string;
  userTo: string;
}

class UpdateUserRelationship {
  public static async perform(req, res): Promise<any> {
    const reqBody: IRequestBody = req.body;
    req.assert('userFrom', cannotBeBlank('User From Id')).notEmpty();
    req
      .assert('relationship', `Relationship status is not valid (not 'like', 'dislike' or 'neutral')`)
      .isIn([Relationship.LIKE, Relationship.DISLIKE, Relationship.NEUTRAL]);
    req.assert('userTo', cannotBeBlank('User To Id')).notEmpty();
    req.assert('userFrom', 'User cannot like themselves').not().equals(reqBody.userTo);

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    try {
      const _userFrom: ObjectId = new ObjectId(reqBody.userFrom);
      let _relationship: string = reqBody.relationship;
      const _userTo: ObjectId = new ObjectId(reqBody.userTo);

      const originalUserPair = await UserRelationship.findOne({ userFrom: _userFrom, userTo: _userTo });
      // TODO: custom 400 error
      if (!originalUserPair) {
        return res.status(400).send(`Users with ids [${reqBody.userFrom}] and/or [${reqBody.userTo}] have not interacted.`);
      }

      _relationship = _relationship === Relationship.LIKE ? await checkForMatch(_userFrom, _userTo) : _relationship;

      let updatedUserPair = await UserRelationship.findOneAndUpdate(
        { userFrom: _userFrom, userTo: _userTo },
        { relationship: _relationship },
        { new: true }
      );

      // Unmatch
      if (originalUserPair.relationship === Relationship.MATCH) {
        await checkForUnmatch(updatedUserPair);
      }

      return res.status(201).json(updatedUserPair);
    } catch (err) {
      Log.error(err.stack);
      return res.status(500).send(err.toString());
    }
  }
}

export default UpdateUserRelationship;
