/**
 * Define Delete User Relationship API
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import { cannotBeBlank } from '../../../utils/methods';
import UserRelationship from '../../../models/UserRelationship';
import Log from '../../../middlewares/Log';
import { ObjectId } from 'bson';

interface IRequestBody {
  userId: string;
}

class DeleteUserRelationship {
  public static async perform(req, res): Promise<any> {
    const reqBody: IRequestBody = req.body;
    req.assert('userId', cannotBeBlank('User Id')).notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    try {
      const _userId: ObjectId = new ObjectId(reqBody.userId);

      await UserRelationship.deleteMany({ userFrom: _userId });
      await UserRelationship.deleteMany({ userTo: _userId });

      return res.status(204).send();
    } catch (err) {
      Log.error(err.stack);
      return res.status(500).send(err.toString());
    }
  }
}

export default DeleteUserRelationship;
