/**
 * TODO: to refactor in the future
 */

import { ObjectId } from 'bson';
import { IUserRelationshipModel } from '../../../models/UserRelationship';
import UserRelationship from '../../../models/UserRelationship';
import { Relationship } from '../../../interfaces/models/userRelationship';

export const checkForMatch = async (userFrom: ObjectId, userTo: ObjectId): Promise<Relationship> => {
  const otherUserPair = await UserRelationship.findOne({ userFrom: userTo, userTo: userFrom });
  if (!otherUserPair || otherUserPair.relationship !== Relationship.LIKE) return Relationship.LIKE;

  const updatedOtherUserPair = await UserRelationship.findOneAndUpdate(
    { userFrom: userTo, userTo: userFrom },
    { relationship: Relationship.MATCH },
    { new: true }
  );

  return Relationship.MATCH;
};

export const checkForUnmatch = async (userPair: IUserRelationshipModel): Promise<void> => {
  const _userFrom: ObjectId = userPair.userFrom;
  const _userTo: ObjectId = userPair.userTo;

  const otherUserPair = await UserRelationship.findOne({ userFrom: _userTo, userTo: _userFrom });
  // TODO: custom 500 error (should not happen)
  if (!otherUserPair || otherUserPair.relationship !== Relationship.MATCH) {
    return;
  }

  const updatedOtherUserPair = await UserRelationship.findOneAndUpdate(
    { userFrom: _userTo, userTo: _userFrom },
    { relationship: Relationship.LIKE },
    { new: true }
  );

  return;
};
