/**
 * Define Get User Relationship API
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import IUserRelationship, { Relationship } from '../../../interfaces/models/userRelationship';
import { cannotBeBlank } from '../../../utils/methods';
import UserRelationship from '../../../models/UserRelationship';
import Log from '../../../middlewares/Log';
import { ObjectId } from 'bson';

interface IRequestBody {
  userFrom: string;
  userTo: string;
}

class GetUserRelationship {
  public static async perform(req, res): Promise<any> {
    const reqBody: IRequestBody = req.body;
    req.assert('userFrom', cannotBeBlank('User From Id')).notEmpty();
    req.assert('userTo', cannotBeBlank('User To Id')).notEmpty();
    req.assert('userFrom', 'User From and User To cannot be the same person').not().equals(reqBody.userTo);

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    try {
      const _userFrom: ObjectId = new ObjectId(reqBody.userFrom);
      const _userTo: ObjectId = new ObjectId(reqBody.userTo);

      let foundUserPair = await UserRelationship.findOne({ userFrom: _userFrom, userTo: _userTo });

      // TODO: custom 400 error
      if (!foundUserPair) {
        return res.status(400).send(`Users with ids [${reqBody.userFrom}] and [${reqBody.userTo}] have not interacted.`);
      }

      return res.status(200).json(foundUserPair);
    } catch (err) {
      Log.error(err.stack);
      return res.status(500).send(err.toString());
    }
  }
}

export default GetUserRelationship;
