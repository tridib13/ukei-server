/**
 * Define Establish User Relationsip API
 *
 * @author Jerald Yik <jeraldyik12@gmail.com>
 */

import IUserRelationship, { Relationship } from '../../../interfaces/models/userRelationship';
import { cannotBeBlank } from '../../../utils/methods';
import UserRelationship from '../../../models/UserRelationship';
import Log from '../../../middlewares/Log';
import { ObjectId } from 'bson';
import User from '../../../models/User';
import { checkForMatch } from './helper';

interface IRequestBody {
  userFrom: string;
  relationship: string;
  userTo: string;
}

class EstablishUserRelationship {
  public static async perform(req, res): Promise<any> {
    const reqBody: IRequestBody = req.body;
    req.assert('userFrom', cannotBeBlank('User From Id')).notEmpty();
    req.assert('relationship', `Relationship status is not valid (not 'like' or 'dislike')`).isIn([Relationship.LIKE, Relationship.DISLIKE]);
    req.assert('userTo', cannotBeBlank('User To Id')).notEmpty();
    req.assert('userFrom', 'User cannot like themselves').not().equals(reqBody.userTo);

    const errors = req.validationErrors();
    if (errors) {
      return res.status(400).send(errors);
    }

    try {
      const _userFrom: ObjectId = new ObjectId(reqBody.userFrom);
      let _relationship: string = reqBody.relationship;
      const _userTo: ObjectId = new ObjectId(reqBody.userTo);

      const foundUserFrom = await User.findOne({ _id: _userFrom });
      const foundUserTo = await User.findOne({ _id: _userTo });
      if (!(foundUserFrom && foundUserTo)) {
        // TODO: custom 400 error
        return res.status(400).send(`Users with ids [${reqBody.userFrom}] and/or [${reqBody.userTo}] do not exist.`);
      }

      const foundUserPair = await UserRelationship.findOne({ userFrom: _userFrom, userTo: _userTo });
      // TODO: custom 400 error
      if (foundUserPair) {
        return res.status(400).send(`User [${_userFrom.toString()}] has already interacted with User [${_userTo.toHexString()}]`);
      }

      _relationship = _relationship === Relationship.LIKE ? await checkForMatch(_userFrom, _userTo) : _relationship;

      const _userPair: IUserRelationship = {
        userFrom: _userFrom,
        relationship: _relationship,
        userTo: _userTo
      };

      const userPairDoc = new UserRelationship(_userPair);
      userPairDoc.save();

      return res.status(201).json(userPairDoc);
    } catch (err) {
      Log.error(err.stack);
      return res.status(500).send(err.toString());
    }
  }
}

export default EstablishUserRelationship;
