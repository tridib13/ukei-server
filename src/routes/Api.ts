/**
 * Define all your API web-routes
 *
 * @author Faiz A. Farooqui <faiz@geekyants.com>
 */

import { Router } from 'express';
import * as expressJwt from 'express-jwt';

import Locals from '../providers/Locals';

import HomeController from '../controllers/Api/Home';
import LoginController from '../controllers/Api/Auth/Login';
import RegisterController from '../controllers/Api/Auth/Register';
import RefreshTokenController from '../controllers/Api/Auth/RefreshToken';
import SendSMSController from '../controllers/Api/Auth/SendSMS';
import VerifyOTPController from '../controllers/Api/Auth/VerifyOTP';

import EstablishUserRelationshipController from '../controllers/Api/UserRelationship/EstablishUserRelationship';
import GetUserRelationshipController from '../controllers/Api/UserRelationship/GetUserRelationship';
import UpdateUserRelationshipController from '../controllers/Api/UserRelationship/UpdateUserRelationship';
import DeleteUserRelationshipController from '../controllers/Api/UserRelationship/DeleteUserRelationship';

const router = Router();
const secret = Locals.config().appSecret;

router.get('/', HomeController.index);

router.post('/auth/login', LoginController.perform);
router.post('/auth/register', RegisterController.perform);
router.post('/auth/refresh-token', expressJwt({ secret }), RefreshTokenController.perform);
router.post('/auth/sendsms', expressJwt({ secret }), SendSMSController.perform);
router.post('/auth/verify', VerifyOTPController.perform);

router.post('/relationship', EstablishUserRelationshipController.perform);
router.get('/relationship', GetUserRelationshipController.perform);
router.patch('/relationship', UpdateUserRelationshipController.perform);
router.delete('/relationship', DeleteUserRelationshipController.perform);

export default router;
