import Locals from '../providers/Locals';
import * as jwt from 'jsonwebtoken';
import { Token } from '../interfaces/vendors';

const signToken = (userId: string) => {
  return jwt.sign({ userId } as Token, Locals.config().appSecret, { expiresIn: Locals.config().jwtExpiresIn * 60 });
};

export { signToken };
