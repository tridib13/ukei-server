# UKEI Server

## Install, Configure & Run

Below mentioned are the steps to install, configure & run in your platform/distributions.

```bash
# Without Docker

# Note: It is presumed here that you have mongoose running in background & you have created the database.

# Install NPM dependencies.
npm install;

# Run the app
npm run dev;
```

```bash
# With Docker

# Note: It is preassumed here that you have docker running in background

# Run the app in docker as a foreground process
npm run docker:start

# Run the app in docker as a background process
docker-compose up -d

# Run the mongo CLI
npm run docker:db
```

Reminder to always run `npm run prettier:fix` before pushing to remote branch

## Frameworks Used

- This app is built using [Express.js](https://expressjs.com/) web framework, and is using [Typescript Lang](https://www.typescriptlang.org/) for writing the app's logic.
- It uses Node's [Cluster API](https://nodejs.org/api/cluster.html), this helps us to take advantage of multi-core systems & to handle the load.
- For storing custom constant configurations within the `process.env` - [DotEnv](https://github.com/motdotla/dotenv) package is used.
- For Database - Repo contains the use of [Mongoose](https://mongoosejs.com/) (ie. [MongoDB](https://www.mongodb.com/) object modeling for [Node.js](https://nodejs.org/en/)).
- For Cache - Repo contains the use of [memory-cache](https://github.com/ptarjan/node-cache#readme) (ie. A simple in-memory cache for node.js).
- For Routing - Repo contains the use of [express-router](https://expressjs.com/en/guide/routing.html) & have distributed Routes into two files ie. Web Routes & API Routes.
- For Route Auth Middleware - Web routes are configured with [CSRF Token](https://github.com/krakenjs/lusca) while the API routes are configured with [JSON Web Token](https://github.com/auth0/express-jwt).
- For Strategies Auth - Repo contains the use of the [Passport.js](https://github.com/jaredhanson/passport). Passport.js is compatible with Express.js and is authentication middleware for Node.js.
- For Logging - Repo uses custom Log class built in middlewares folder, and it creates logs file by date & removes the log files after 'X' days (You can define that 'X' in the `.env` file).
- For Handling Exception - Repo contains two classes ie. `Handler` & `NativeEvent`.
- To Log - use `Log.info('Your message should go here!')`. Other options for logging are `Log.warn`, `Log.error` & `Log.custom`.
- For views - Repo contains the use of [PUG](https://github.com/pugjs/pug) template engine.
- For background queues - Repo contains the use of [Kue](https://github.com/Automattic/kue). For more details, please review the [Queue](https://github.com/faizahmedfarooqui/nodets/blob/master/src/providers/Queue.ts) class.

# Global Requisites

- node (>= 10.5.0)
- tsc (>= 3.0.1)
- typescript (>= 3.0.1)
- mongoose (>= 3.6.2)
- redis

# App Structure

> _Note: I am mentioning only files/folders which you need to configure if required_

```bash
├── dist
├── public
├── src
│   ├── controllers
│   │   ├── Api
│   │   │   ├── Auth
│   │   │   │   ├── Login.ts
│   │   │   │   ├── RefreshToken.ts
│   │   │   │   └── Register.ts
│   │   │   └── Home.ts
│   │   ├── Auth
│   │   │   ├── Login.ts
│   │   │   ├── Logout.ts
│   │   │   ├── Register.ts
│   │   │   └── Social.ts
│   │   ├── Account.ts
│   │   └── Home.ts
│   ├── exception
│   │   ├── Handler.ts
│   │   └── NativeEvent.ts
│   ├── interfaces
│   │   ├── models
│   │   │   └── user.ts
│   │   └── vendors
│   │        ├── index.ts
│   │        ├── INext.ts
│   │        ├── IRequest.ts
│   │        └── IResponse.ts
│   ├── middlewares
│   │   ├── CORS.ts
│   │   ├── CsrfToken.ts
│   │   ├── Http.ts
│   │   ├── Kernel.ts
│   │   ├── Log.ts
│   │   ├── Statics.ts
│   │   ├── StatusMonitor.ts
│   │   └── View.ts
│   ├── models
│   │   └── User.ts
│   ├── providers
│   │   ├── App.ts
│   │   ├── Cache.ts
│   │   ├── Database.ts
│   │   ├── Express.ts
│   │   ├── Locals.ts
│   │   ├── Passport.ts
│   │   ├── Queue.ts
│   │   └── Routes.ts
│   ├── routes
│   │   ├── Api.ts
│   │   └── Web.ts
│   ├── services
│   │   └── strategies
│   │        ├── Google.ts
│   │        ├── Local.ts
│   │        └── Twitter.ts
│   └── index.ts
├── views
│   ├── includes
│   ├── modals
│   ├── pages
│   ├── partials
│   ├── static
│   │   ├── css/*.css
│   │   └── js/*.js
│   └── layout.pug
├── .env
├── .gitignore
├── .eslintrc
├── nodemon.json
├── package.json
├── README.md
├── tsconfig.json
└── tslint.json
```

# List of Routes

```sh
# Web Routes:

+--------+-------------------------+
  Method | URI
+--------+-------------------------+
  GET    | /
  GET    | /signup
  POST   | /signup
  GET    | /login
  POST   | /login
  GET    | /logout
  GET    | /account
  GET    | /auth/google
  GET    | /auth/google/callback
  GET    | /auth/twitter
  GET    | /auth/twitter/callback
  GET    | /status-monitor
+--------+-------------------------+

# API Routes:

+--------+-------------------------+
  Method | URI
+--------+-------------------------+
  POST   | /api/auth/login
  POST   | /api/auth/register
  POST   | /api/auth/refresh-token
  POST   | /api/auth/sendsms
  POST   | /api/auth/verify
         |
  POST   | /api/profile/:id
  GET    | /api/profile/:id
  PUT    | /api/profile/:id
  DELETE | /api/profile/:id
         |
  POST   | /api/question
  GET    | /api/question
  GET    | /api/question/:id
  PATCH  | /api/question/:id
  DELETE | /api/question/:id
         |
  POST   | /api/answer
  GET    | /api/answer/user
  GET    | /api/answer/question
  PATCH  | /api/answer
  DELETE | /api/answer
         |
  POST   | /api/relationship
  GET    | /api/relationship
  PATCH  | /api/relationship
  DELETE | /api/relationship
+--------+-------------------------+
```
